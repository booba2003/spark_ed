import datetime

from flask import json
from flask.testing import FlaskClient
from sqlalchemy import or_, null

from app.models.products import Category, Product, Brand


def __init_data(session):
    categories = [
        Category(name="c1"),
        Category(name="c2"),
        Category(name="c3"),
        Category(name="c4"),
        Category(name="c5"),
        Category(name="c6"),
        Category(name="c7"),

        Brand(name='b1', country_code='cc1'),
        Brand(name='b2', country_code='cc2'),
        Brand(name='b3', country_code='cc3'),
        Brand(name='b4', country_code='cc4'),
        Brand(name='b5', country_code='cc5'),
        Brand(name='b6', country_code='cc6'),
    ]

    session.bulk_save_objects(categories)
    session.commit()


def test_create_product(client: FlaskClient, db):
    __init_data(db.session)

    product_source = {
        'brand': {
            'id': 1,
        },
        'categories': [1, 2, 3, 4, 5],
        'created_at': '2020-05-19T10:28:30',
        'expiration_date': '2020-01-01T10:28:30',
        'items_in_stock': 2,
        'name': 'product name',
        'rating': 1.0,
        'receipt_date': None
    }

    resp = client.post('/product/', json=product_source, follow_redirects=True)

    assert resp.status_code == 200

    product = Product.query.first()

    assert product


def test_delete_product(client: FlaskClient, db):
    __init_data(db.session)

    product_source = {
        'brand': {
            'id': 1,
        },
        'categories': [1, 2, 3, 4, 5],
        'created_at': '2020-05-19T10:28:30',
        'expiration_date': '2020-01-01T10:28:30',
        'items_in_stock': 2,
        'name': 'product name',
        'rating': 1.0,
        'receipt_date': None
    }

    resp = client.post('/product/', json=product_source, follow_redirects=True)

    product_stored = json.loads(resp.data)

    product_id = product_stored.get("result", {}).get('id')

    resp = client.delete(f'/product/{product_id}/')

    assert resp.status_code == 200

    result_deletion = json.loads(resp.data)
    assert result_deletion.get('result') == 1


def test_update_product(client: FlaskClient, db):
    __init_data(db.session)

    product_source = {
        'brand': {
            'id': 1,
        },
        'categories': [1, 2, 3, 4, 5],
        'created_at': '2020-05-19T10:28:30',
        'expiration_date': '2020-01-01T10:28:30',
        'items_in_stock': 2,
        'name': 'product name',
        'rating': 1.0,
        'receipt_date': None
    }

    resp = client.post('/product/', json=product_source, follow_redirects=True)

    product_stored = json.loads(resp.data)

    product_id = product_stored.get("result", {}).get('id')

    product_update_source = {
        'brand': {
            'id': 1,
        },
        'categories': [1, 2, 3, 4, 5],
        'created_at': '2020-05-19T10:28:30',
        'expiration_date': '2020-01-01T10:28:30',
        'items_in_stock': 2,
        'name': 'product name changed',
        'rating': 1.0,
        'receipt_date': None
    }

    resp = client.put(f'/product/{product_id}/', json=product_update_source)
    product_updated = json.loads(resp.data)

    assert resp.status_code == 200
    assert product_updated.get('result', {}).get('name') == product_update_source.get('name')


def test_get_products(client: FlaskClient, db):
    __init_data(db.session)

    client.post('/product/', json={
        'brand': {
            'id': 1,
        },
        'categories': [1, 2, 3, 4, 5],
        'created_at': '2020-05-19T10:28:30',
        'expiration_date': '2020-01-01T10:28:30',
        'items_in_stock': 2,
        'name': 'product name',
        'rating': 1.0,
        'receipt_date': None
    }, follow_redirects=True)
    client.post('/product/', json={
        'brand': {
            'id': 1,
        },
        'categories': [1, 2, 3, 4, 5],
        'created_at': '2020-05-19T10:28:30',
        'items_in_stock': 2,
        'name': 'product name',
        'rating': 1.0,
        'receipt_date': None
    }, follow_redirects=True)

    resp = client.get('/products/', follow_redirects=True)
    product_list = json.loads(resp.data).get('result', [])

    delta = datetime.datetime.now() - datetime.timedelta(days=30)
    products_query = Product.query.filter(or_(Product.expiration_date == null(), Product.expiration_date > delta))

    assert len(product_list) == products_query.count()


def test_get_product(client: FlaskClient, db):
    __init_data(db.session)

    product_created_result = client.post('/product/', json={
        'brand': {
            'id': 1,
        },
        'categories': [1, 2, 3, 4, 5],
        'created_at': '2020-05-19T10:28:30',
        'items_in_stock': 2,
        'name': 'product name',
        'rating': 1.0,
        'receipt_date': None
    }, follow_redirects=True)
    product_created = json.loads(product_created_result.data)
    product_id = product_created.get("result", {}).get('id')

    product_detailed_result = client.get(f'/product/{product_id}/', follow_redirects=True)

    assert product_detailed_result.status_code == 200
