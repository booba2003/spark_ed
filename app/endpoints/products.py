import datetime
from http.client import NOT_FOUND

from flask import Blueprint, jsonify, request, abort
from sqlalchemy import or_, null

import app
from app.models.products import Product
from app.schemas.products import product_schema, product_create_schema

products_blueprint = Blueprint('products', __name__)


@products_blueprint.route('/products/', methods=['GET'])
def get_products():
    delta = datetime.datetime.now() - datetime.timedelta(days=30)
    products = Product.query.filter(or_(Product.expiration_date == null(), Product.expiration_date > delta)).all()
    return jsonify({
        'result': product_schema.dump(products, many=True)
    })


@products_blueprint.route('/product/<product_id>/', methods=['GET'])
def get_product(product_id):
    product = Product.query.get(product_id)
    if product:
        return jsonify({
            'result': product_schema.dump(product)
        })
    else:
        return abort(NOT_FOUND)


@products_blueprint.route('/product/<product_id>/', methods=['PUT'])
def update_product(product_id):
    product = product_create_schema.load(request.json, instance=Product.query.get(product_id))

    if product:
        app.db.session.add(product)
        app.db.session.commit()
        return jsonify({
            'result': product_schema.dump(product)
        })
    else:
        return abort(NOT_FOUND)


@products_blueprint.route('/product/', methods=['POST'])
def create_product():
    product = product_create_schema.load(request.json)
    app.db.session.add(product)
    app.db.session.commit()

    return jsonify({
        'result': product_schema.dump(product)
    })


@products_blueprint.route('/product/<product_id>/', methods=['DELETE'])
def delete_product(product_id):
    product = Product.query.get(product_id)
    if product:
        product.categories = []
        app.db.session.commit()

        deletion_result = Product.query.filter(Product.id == product_id).delete()
        app.db.session.commit()
        return jsonify({
            'result': deletion_result
        })
    else:
        return abort(NOT_FOUND)
