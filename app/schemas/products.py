import datetime

from marshmallow import fields
from marshmallow.validate import Length
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

import app
from app.models.products import Product, Brand, Category


class BaseSchema(SQLAlchemyAutoSchema):
    class Meta:
        sqla_session = app.db.session


class BrandSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = Brand
        include_relationships = True
        load_instance = True


class CategorySchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = Category
        include_relationships = True
        load_instance = True


class ProductSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = Product
        include_fk = True
        load_instance = True

        exclude = ("brand_id",)

    brand = fields.Nested(BrandSchema, required=True, only=("id", "name", "country_code"))
    categories = fields.Pluck(CategorySchema, 'id', many=True, required=True, validate=Length(min=1, max=5))
    expired = fields.Function(lambda obj: ((datetime.datetime.now() - obj.expiration_date).days > 30 if obj.expiration_date else False), dump_only=True)
    featured = fields.Function(lambda obj: obj.rating > 8, dump_only=True)


class ProductCreateSchema(ProductSchema):
    brand = fields.Nested(BrandSchema, required=True, only=("id",))


product_schema = ProductSchema()
product_create_schema = ProductCreateSchema()
